# FlyBac
###### Authors: Jürgen W. Schönborn[1][2], Fiona A. Stewart[1][2], Kerstin Maas Enriquez[1][2], Irfan Akhtar[1][2], Andrea Droste[1][2], Silvio Waschina[3], and Mathias Beller[1][2]

[1]\
Institut für Mathematische Modellierung Biologischer Systeme\
Heinrich-Heine-Universität Düsseldorf\
40225 Düsseldorf, Germany\
[2]\
Systembiologie des Fettstoffwechsels\
Heinrich-Heine-Universität Düsseldorf\
40225 Düsseldorf, Germany\
[3]\
Christian-Albrechts-University Kiel\
Institute of Human Nutrition and Food Science, Nutriinformatics\
Heinrich-Hecht-Platz 10, 24118 Kiel, Germany\

**Code deposit for bacterial growth and analysis/plotting**

Also available at Mendeley Data:

> Schönborn, Jürgen; Stewart, Fiona; Enriquez, Kerstin; Akhtar, Irfan ; Droste, Andrea; Waschina, Silvio; Beller, Mathias (2021), “Dmel_Microbiome_GSMN”, Mendeley Data, V1, doi: 10.17632/2tgjd6y4zb.1   

-- From https://gitlab.com/Beller-Lab --
